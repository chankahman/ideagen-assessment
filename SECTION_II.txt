Q1:

<?php

function countDoubleContiguous($str) {

    $characters = str_split($str);
    
    $count = 0;
    
    foreach($characters as $charIndex => $char) {
    	if(!isset($characters[$charIndex + 2])) {
    		break;
    	}
    	if(isset($characters[$charIndex - 1]) && $characters[$charIndex - 1] === $char) {
    		continue;
    	}
    	if($characters[$charIndex + 1] === $char && $characters[$charIndex + 2] !== $char) {
    		$count++;
    	}
    }
    
    return $count;

}

Q2:

<?php

function calculateVowelConsonants($arr) {
	
	$vowels = array_filter($arr, function($char) {
		
		return !in_array($char, ['a','e','i','o','u']);
		
	});
	
	return [
		'vowels' => count($vowels),
		'consonants' => count($arr) - count($vowels)
	];
	
}


Q3:

<?php

function printPattern($int) {
	
	$numbers = range(1, $int);
	
	for($i = 0 ; $i < $int ; $i++) {
		
		echo implode(' ', $numbers) . '<br><br>';
	
		$numbers[] = $numbers[0];
		
		array_shift($numbers);
		
	}
}

Q4: ORM & DB manipulation module of Laravel framework.
Q5: Authentication mechanism, reject requests which do not bring valid authenticated keys.
Q6: Give a name to route, it can be used when generateing route url url('nameOfRoute')